﻿namespace PresenceApi.Access;

public class SimulatedAccessSystem : IAccessSystem
{
    public Task<byte> GetEmployeeCountAsync()
    {
        var employeeCount = Convert.ToByte(DateTime.Now.Hour);
        return Task.FromResult(employeeCount);
    }
}