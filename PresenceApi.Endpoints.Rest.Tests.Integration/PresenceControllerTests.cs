using System.Net.Http.Json;
using FluentAssertions;

namespace PresenceApi.Endpoints.Rest.Tests.Integration;

public class PresenceControllerTests : IClassFixture<WebApplicationFixture>
{
    private readonly WebApplicationFixture _webApplicationFixture;

    public PresenceControllerTests(WebApplicationFixture webApplicationFixture)
    {
        this._webApplicationFixture = webApplicationFixture;
    }

    private HttpClient HttpClient => this._webApplicationFixture.HttpClient;

    [Fact]
    public async Task Presence_ReturnsPresenceNumber()
    {
        var response = await this.GetPresenceAsync();
        response.Should().BeSuccessful();

        var presence = await ReadPresenceFromResponseAsync(response);
        presence.Should().BePositive().And.BeGreaterThan(0);
    }

    [Theory]
    [InlineData(byte.MinValue)]
    [InlineData(byte.MaxValue)]
    public async Task Echo_EchoesGivenNumber(byte expectedPresence)
    {
        var response = await this.GetPresenceEchoAsync(expectedPresence);
        response.Should().BeSuccessful();

        var presence = await ReadPresenceFromResponseAsync(response);
        presence.Should().Be(expectedPresence);
    }

    private static Task<byte> ReadPresenceFromResponseAsync(HttpResponseMessage response) => response
        .Content
        .ReadFromJsonAsync<byte>();

    private Task<HttpResponseMessage> GetPresenceEchoAsync(byte presence)
    {
        var uri = new UriBuilder
        {
            Path = KnownEndpoints.Presence.Echo,
            Query = $"presence={presence}"
        }.Uri;

        return this.HttpClient.GetAsync(uri);
    }

    private Task<HttpResponseMessage> GetPresenceAsync() => this.HttpClient.GetAsync(
        KnownEndpoints.Presence.Get
    );
}