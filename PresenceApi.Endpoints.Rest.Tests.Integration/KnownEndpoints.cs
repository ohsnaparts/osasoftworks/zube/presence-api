
namespace PresenceApi.Endpoints.Rest.Tests.Integration;

internal static class KnownEndpoints
{
    internal class Presence
    {
        private const string RootPath = "/Presence";
        public const string Get = $"{RootPath}";
        public const string Echo = $"{RootPath}/Echo";
    }
    
    
}