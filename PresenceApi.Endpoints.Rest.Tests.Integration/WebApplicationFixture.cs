using Microsoft.AspNetCore.Mvc.Testing;

// ReSharper disable once ClassNeverInstantiated.Global
namespace PresenceApi.Endpoints.Rest.Tests.Integration;

public class WebApplicationFixture : IAsyncLifetime
{
    private HttpClient? _webAppClient;

    public HttpClient HttpClient
    {
        private set => this._webAppClient = value;
        get
        {
            ArgumentNullException.ThrowIfNull(this._webAppClient, nameof(this._webAppClient));
            return this._webAppClient;
        }
        
    }

    public Task InitializeAsync()
    {
        this.HttpClient = new WebApplicationFactory<Program>().CreateClient();
        return Task.CompletedTask;
    }

    public Task DisposeAsync()
    {
        this.HttpClient.Dispose();
        return Task.CompletedTask;
    }
}