# Presence Api

Exposes presence data via API

## Context

The presence bot is developed for use in our company premisses. To retrieve
presence data, we have to issue complex calls to internal services that.

For resons:

1. Exposing these to the public is dangerous
1. The presence-bot infrastructure should not have to rely on implementation details

the actual presence api is kept in a closed-source on-premise while this application
implements the same interfaces but provides mere test data.

A "lets pretend" implementation if you will.


## Requirements

- .NET 6.0.301
- Docker version 20.10.17
- Docker Compose version v2.6.0

## Usage

Spin up the application using

```sh
./run.sh
```

and access `/swagger` to find all available endpoints.
You can look up the port in the `docker-compose.yml` file.

## License

See the [LICENSE](./LICENSE) in the project root directory.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
