using PresenceApi.Builder;

public class PresenceApiTests
{
    [Fact]
    public async Task Host_Executable()
    {
        var host = WebApplicationHostBuilder.Build(Array.Empty<string>());
        await host.StartAsync();
        await host.StopAsync();
    }
}