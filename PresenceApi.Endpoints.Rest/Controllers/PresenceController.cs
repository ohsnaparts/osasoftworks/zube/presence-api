using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PresenceApi.UseCases.Abstractions;

namespace PresenceApi.Endpoints.Rest.Controllers;

[ApiController]
[Route("[controller]")]
public class PresenceController : ControllerBase
{
    private readonly ILogger<PresenceController> _logger;
    private readonly IMediator _mediator;

    public PresenceController(ILogger<PresenceController> logger, IMediator mediator)
    {
        this._logger = logger;
        this._mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<byte>> Get()
    {
        var presence = await this._mediator.Send(new GetPresenceQuery());
        this._logger.LogDebug("Retrieved presence {@Presence}", presence);
        return this.Ok(presence);
    }

    [HttpGet("Echo")]
    public ActionResult<byte> Echo(byte presence)
    {
        return this.Ok(presence);
    }

}