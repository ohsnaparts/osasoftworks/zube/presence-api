using MediatR;

namespace PresenceApi.UseCases.Abstractions;

// ReSharper disable once ClassNeverInstantiated.Global
public record GetPresenceQuery : IRequest<byte>;