#!/bin/bash

_script_file_path=$(readlink -f "$0")
_solution_dir_path=$(dirname "$_script_file_path")
_compose_file="$_solution_dir_path/docker-compose.yml"

docker compose --file "$_compose_file" up
