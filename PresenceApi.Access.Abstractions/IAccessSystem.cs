namespace PresenceApi.Access;

public interface IAccessSystem
{
    Task<byte> GetEmployeeCountAsync();
}