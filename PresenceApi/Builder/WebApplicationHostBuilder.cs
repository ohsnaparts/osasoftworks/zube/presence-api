using MediatR;
using PresenceApi.Access;
using PresenceApi.UseCases;
using PresenceApi.UseCases.Abstractions;

namespace PresenceApi.Builder;

internal static class WebApplicationHostBuilder
{
    public static IHost Build(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        ConfigureServices(builder.Services);

        var app = builder.Build();
        ConfigureWebApplication(app);

        return app;
    }

    private static void ConfigureWebApplication(WebApplication app)
    {
        app.UseSwagger();
        app.UseSwaggerUI();
        
        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();
    }

    private static void ConfigureServices(IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddEndpointsApiExplorer()
            .AddSwaggerGen()
            .AddMediatR(typeof(GetPresenceQuery).Assembly, typeof(GetPresenceQueryHandler).Assembly)
            .AddTransient<IAccessSystem, SimulatedAccessSystem>()
            .AddControllers();
    }
}