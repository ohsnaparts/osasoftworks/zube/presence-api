using System.Runtime.CompilerServices;

[assembly:InternalsVisibleTo("PresenceApi.Tests.Integration")]
[assembly:InternalsVisibleTo("PresenceApi.Endpoints.Rest.Tests.Integration")]