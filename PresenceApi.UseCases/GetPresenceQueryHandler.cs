﻿using MediatR;
using PresenceApi.Access;
using PresenceApi.UseCases.Abstractions;

namespace PresenceApi.UseCases;

public class GetPresenceQueryHandler : IRequestHandler<GetPresenceQuery, byte>
{
    private readonly IAccessSystem _accessSystem;

    public GetPresenceQueryHandler(IAccessSystem accessSystem)
    {
        this._accessSystem = accessSystem;
    }

    public Task<byte> Handle(
        GetPresenceQuery request,
        CancellationToken cancellationToken
    ) => this._accessSystem.GetEmployeeCountAsync();
}